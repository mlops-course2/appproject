## Сборка и запуск проекта
Для сборки и запуска проекта необходимо установить (если ещё не установлен) docker.
Например со страницы  https://www.docker.com/products/docker-desktop/

Далее создать папку проекта, положить в неё файлы dockerfile и pixi.toml 


pixi.toml в данном случае является заменой файла project.toml

Далее запустить командную оболочку, перейти в папку которую создали ранее и выполнить команду
docker build -t appproject (можно вместо appproject указать что-либо иное)

после сборки образа запускаем команду:

docker volume create pixi_volume

далее:

docker run -it -p 8080:8080 --gpus all -v <путь к папке проекта, включая её название>:/appproject -v pixi_volume:/appproject/.pixi --name myappcontainer --rm -u 0 appproject
например:
docker run -it -p 8080:8080 --gpus all -v /mnt/t/projs/appproject:/appproject -v pixi_volume:/appproject/.pixi --name myappcontainer --rm -u 0 appproject

(то есть подпапку .pixi мы мапим в docker volume, а не напрямую к диску так как она почему-то вызывает ошибку при прямом маппинге)

Далее потребуется подождать некоторое время (возможно что немалое, например минут 5-10). За это время 
менеджер пакетов pixi внутри docker-контейнера соберёт все нужные пакеты и запустит jupyter server

После чего можно зайти в браузер, ввести 127.0.0.1/lab
http://127.0.0.1:8080/lab?token=12345678

