FROM pytorch/pytorch
RUN apt-get update && apt-get install -y curl bash

# Установка pixi
RUN mkdir /appproject
WORKDIR /root
RUN curl -fsSL https://pixi.sh/install.sh | bash

# Добавляем PATH явно, потому что Docker не использует логин-шелл по умолчанию
ENV PATH="/root/.pixi/bin:${PATH}"

# Проверяем, правильно ли установлен pixi
RUN echo $PATH
RUN ls -la /root/.pixi/bin/
RUN which pixi || echo "pixi not found in PATH"

RUN mkdir -p /appproject/.pixi

WORKDIR /appproject
COPY pixi.toml /appproject
#RUN pixi init /appproject
WORKDIR /appproject

CMD ["pixi", "run", "-e", "dev", "jupyter", "lab", "--ip=0.0.0.0", "--port=8080","--no-browser", "--allow-root", "--ServerApp.token=12345678"]
